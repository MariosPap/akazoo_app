package com.example.mpapad.cs_akazoo_app.features.tracks.domain;

import com.example.mpapad.cs_akazoo_app.features.tracks.presentation.TrackUI;

public interface OnTrackClickListener {

    void onTrackClick(TrackUI track);

    void onTrackLogoClick(TrackUI track);

}
