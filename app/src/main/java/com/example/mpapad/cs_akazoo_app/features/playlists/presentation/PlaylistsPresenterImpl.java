package com.example.mpapad.cs_akazoo_app.features.playlists.presentation;

import android.content.Context;

import com.example.mpapad.cs_akazoo_app.R;
import com.example.mpapad.cs_akazoo_app.features.playlists.data.PlaylistsInteractorImpl;
import com.example.mpapad.cs_akazoo_app.features.playlists.domain.PlaylistDomain;
import com.example.mpapad.cs_akazoo_app.features.playlists.domain.PlaylistsInteractor;
import com.example.mpapad.cs_akazoo_app.features.playlists.domain.PlaylistsPresenter;
import com.example.mpapad.cs_akazoo_app.features.playlists.domain.PlaylistsView;

import java.util.ArrayList;

public class PlaylistsPresenterImpl implements PlaylistsPresenter,
        PlaylistsInteractor.OnPlaylistsFinishListener {

    PlaylistsView playlistsView;
    PlaylistsInteractor interactor;

    public PlaylistsPresenterImpl(PlaylistsView playlistsView) {
        this.playlistsView = playlistsView;
        interactor = new PlaylistsInteractorImpl();
    }

    @Override
    public void getPlaylists(Context ctx, Boolean refresh) {
        interactor.getPlaylists(this, ctx, refresh);
    }

    @Override
    public void getFilteredPlaylists(String filterString) {
        interactor.getFilteredPlaylists(this, filterString);
    }


    @Override
    public void onSuccess(ArrayList<PlaylistDomain> playlists) {
        ArrayList<PlaylistUI> playlistsUI = new ArrayList<>();
        if (playlists != null && !playlists.isEmpty()) {
            for (PlaylistDomain playlist : playlists) {
                PlaylistUI playlistUI = new PlaylistUI(
                        playlist.getPlaylistId(),
                        playlist.getName(),
                        playlist.getItemCount(),
                        playlist.getPhotoUrl()
                );

                if (playlistUI.getItemCount() > 40)
                    playlistUI.setColorId(R.color.red);
                else
                    playlistUI.setColorId(R.color.blue);
                playlistsUI.add(playlistUI);
            }
        }

        playlistsView.showPlaylists(playlistsUI);
    }

    @Override
    public void onError() {
        playlistsView.showGeneralError();
    }
}
