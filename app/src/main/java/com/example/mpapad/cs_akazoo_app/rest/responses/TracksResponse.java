package com.example.mpapad.cs_akazoo_app.rest.responses;

public class TracksResponse {

    private TracksResponseItem Result;

    public TracksResponseItem getResult() {
        return Result;
    }

    public void setResult(TracksResponseItem result) {
        Result = result;
    }
}
