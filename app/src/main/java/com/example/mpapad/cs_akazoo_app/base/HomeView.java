package com.example.mpapad.cs_akazoo_app.base;

import android.os.Parcelable;

import com.example.mpapad.cs_akazoo_app.features.playlists.presentation.PlaylistUI;

import java.io.Serializable;

public interface HomeView extends Serializable{

     void addTracksFragment(PlaylistUI playlist);

     void addPlaylistFragment();
}
