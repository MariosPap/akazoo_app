package com.example.mpapad.cs_akazoo_app.features.playlists.domain;

import com.example.mpapad.cs_akazoo_app.features.playlists.presentation.PlaylistUI;

public interface OnPlaylistClickListener {

    void onPlaylistClicked(PlaylistUI playlist);

}
